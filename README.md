# useRinfoboard

Information dashboard providing facts and figures on past useR! conferences

Find the developing dashboard here: https://rconf.gitlab.io/userinfoboard/

# Contributing

You are welcome to contribute data and code to this project. The data for this project is maintained in a collection of comma separated values (CSV) files that are located in the [data](data) directory.

If you find missing data, typos, mispelt names or you have more data, please send a merge request with your contributions to the relevant .csv file in the `data` directory.

This is an ongoing effort, and even if your data is not complete, we are still happy to review your merge request.

We welcome past conference organizers to support us with any missing data that they might still have.

# Code

The information board is built using mainly R, `flexdashboard` and other R packages.

The R Markdown file `index.Rmd` contains calls to functions that are located in different scripts in the `R` folder. All tables and charts and processing code are located within these scripts. R packages that the dashboard depends on are found in the `DESCRIPTION` file and we use the `pkgload` package to load all packages and scripts.  

To update the content on the dashboard requires adding data to any of the CSV files in the data directory and _knitting_ `index.Rmd` which produces a new `index.html` dashboard. The functions in the `R` folder could also be modified and `index.html` re-rendered to produce a desired outcome on the dashboard.

HTML, JavaScript and CSS resources are located in the `includes` folder.
The sidebar menu is maintained separately in a `menu.html` file that is included in the YAML header of `index.Rmd`.

There is a Gitlab Pages pipeline that serves the `index.html` page at this URL: https://rconf.gitlab.io/userinfoboard/

# Issues

If you find issues with the data or the user interface, please file an issue here: https://gitlab.com/rconf/userinfoboard/-/issues
