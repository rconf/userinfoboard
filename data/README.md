# The Data

The information board automatically collects its data from this directory and any information added to the files contained here will be rendered on the information board once the `index.Rmd` file is rendered into a HTML file.

The table below describes the files within this data directory and the years for which each dataset covers.

| Data File | Description | Summary |
| ---	    | ---	  | ---	    |
| childcare.csv | childcare price information | 2017 - 2019 |
| conferences.csv | useR! conference location data | 2004 - 2022, 2024 |
| discount.csv | discount information based on participant location | 2021 - 2022 |
| ethnicity.csv | ethnic information of participants | 2016 - 2022 |
| key_dates.csv | information about key dates | 2016 - 2022, 2024 |
| keynotes.csv | useR! keynotes | 2004 - 2022 |
| organizing_committee_members.csv | organizing committee member information | 2016 - 2022 |
| modified_gender.csv | gender information of participants | 2016 - 2020 |
| participant_numbers.csv | count of participants | 2016 - 2021 |
| participation_country.csv | countries of participants | 2016 - 2021 |
| program.csv | useR! Program data | 2016 - 2021 |
| program_committee_members.csv | program committee member information | 2004 - 2022, 2024 |
| registration_types.csv | count of participants by profession | 2016 - 2021 |
| registration_fees.csv | registration fees | 2016-2019, 2021 |
| sponsors.csv | data on useR! sponsors | 2014 - 2022 |
| tutorials.csv | useR! tutorials | 2017 - 2022 |

## Dates

Key dates are in ISO8601 format, i.e., "YYYY-MM-DD". Opening in Excel may 
change the format - either format the cells with the Custom format "yyyy-mm-dd" 
before saving the CSV, or use an editor that keeps the format, like 
[Modern CSV](https://www.moderncsv.com/) or [DataEditR](https://cran.r-project.org/package=DataEditR).

## Notes for specific data files

### Childcare 

* This table only includes years where childcare was offered at the conference
* The price was the same for all children, regardless of age, in 2018 and 2019

### Conferences 

Please map to existing categories:

* `Format`: Online, In-person, Hybrid


### Ethnicity

Please map to existing categories:

* `tidy_ethnicity`: Asian or Pacific Islander, Black/African/Caribbean, Hispanic/Latinx, Middle-Eastern/North-African, Mixed/Multiple ethnic groups, White, NA
* `Count`: For privacy reasons, an arbitrary number of 2.5 is used where the count is 5 or below


### Key dates

Please map to one of previously defined options, or adapt to use the same key words/phrases:

|Action                                                                                             |
|:--------------------------------------------------------------------------------------------------|
|Abstract selection and diversity scholarship notifications                                         |
|Abstract selection notifications                                                                   |
|Abstract submission and diversity scholarship application closes                                   |
|Abstract submission closes                                                                         |
|Abstract submission opens                                                                          |
|Abstract submission opens for regular talks, panels, and posters                                   |
|Abstract submission opens for regular talks, panels, incubators and elevator pitches               |
|Abstract submission opens for tutorials and presentations; diversity scholarship application opens |
|ASA grant application closes                                                                       |
|Conference closes                                                                                  |
|Conference opens                                                                                   |
|Datathon entry closes                                                                              |
|Deadline for additional AV requests                                                                |
|Deadline for selected contributors to send posters and optional elevator pitch                     |
|Deadline for selected contributors to send us videos (virtual only)                                |
|Deadline for selected contributors to send us videos, technical notes and transcripts              |
|Deadline for slides                                                                                |
|Diversity scholarship application closes                                                           |
|Diversity/needs-based scholarship application closes                                               |
|Diversity/needs-based scholarship application opens                                                |
|Early bird rates are extended until registration closes                                            |
|Early bird registration closes                                                                     |
|Late registration closes                                                                           |
|Poster submission closes                                                                           |
|Registration closes                                                                                |
|Registration opens                                                                                 |
|Regular registration closes                                                                        |
|Schedule announced                                                                                 |
|Tutorial day                                                                                       |
|Tutorial selection notifications                                                                   |
|Tutorial submission closes                                                                         |
|Tutorial submission opens                                                                          |
|Website online                                                                                     |
|Young academics scholarship application closes                                                     |

### Keynotes

About the links in the `video` column:

* In the years 2014, 2019-2022 the link provided will take you to a playlist, where individual keynote recordings are located
* In the year 2018, the link provided will take you to directly to a recording of the specified keynote speech


### Modified Gender

Please map to existing categories:
  
* `Type`: Man, Woman, Gender-Diverse
* `Count`: For privacy reasons, an arbitrary number of 2.5 is used where the count is 5 or below


### Organizing Committee Members

* Please make sure every entry in the `Name` column is in capital case, i.e. `Heather Turner`

### Participation Country 

Please map to existing categories:
  
* `Continent`: Africa, Americas, Asia, Europe, Oceania
* `iso3_code`, `region`, `income`: These must be found using the [wbstats package](https://cran.r-project.org/web/packages/wbstats/vignettes/wbstats.html)


### Program 

Please map to existing categories:
  
* `Presentation_type`: Regular talk, Panel, Lightning talk, Poster/tech note, Incubator, Invited talk


### Program Committee Members

* Please make sure every entry in the `name` column is in capital case, i.e. `Heather Turner`


### Registration fees 

Please map to existing categories:
  
* `Reg_type`: Early, Regular, Late, On-site
* `Participant_type`: Student, Academic, Industry, Non-profit

Additional notes: 

* `Tutorial_fees` and `Conf_dinner` are `0` in years where the conference registration fees included the cost of tutorials and/or the conference dinner
* The `Conf_dinner` is `NA` in years where the conference was virtual and a dinner was not held. 
* There was no conference in 2020, so everything is `NA` in these rows

### Tutorials

* The `duration` column is measured in minutes
* There should be at most 3 descriptors in the `tags` column 


## License

The data is provided under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license
